import sys


def thirdmax(nums):

    nums = list(set(nums))
    max1 = nums[0]
    max2 = -sys.maxsize
    max3 = -sys.maxsize
    for num in nums:

        if num > max1:
            max3 = max2
            max2 = max1
            max1 = num
        elif num > max2:
            max3 = max2
            max2 = num
        elif num > max3:
            max3 = num

    if len(nums) == 2 or len(nums) == 1:
        return max1

    return max3


print(thirdmax([1, 1, 1]))
