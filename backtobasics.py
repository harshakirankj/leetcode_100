
a = [1, 2, 2, 2, 2, 2, 4, 5, 6, 7, 8, 9, 44, 3, 22, 76, 43, 22, 29]
b = a
c = a.copy()
for i in range(len(a)):
    print(a[i])

for i in a:
    print(i)

print(a)
print(a[::1])
print(a[::2])
print(a[::-3])


print(a)
a[1] = 10000
a[2] = 999

print(a)
print(b)

if 999 in a:
    print("hurar")

print(a.pop())
del a[1]
print(a)
print(b)
a.remove(999)
print(a)
print(b)

a.extend(b)
print(a)
print(c)
print("a+c", a + c)

print(b)
b.remove(43)
print(b)

######################################################
set1 = {"a", "b", "c"}
set2 = {1, 2, 3}

set1.update(set2)
print(set1)
