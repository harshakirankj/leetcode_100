# https://medium.com/datadriveninvestor/array-rotation-o-1-space-solution-with-proof-of-correctness-c5af5713fe7c


###############################################################################
# Extra space O(n)
from collections import deque


def rotatearraybyk(nums, i):
    res = []
    k = i
    if i == 0:
        return nums
    while (k != 0):
        res.append(nums[len(nums) - k])
        k -= 1
    for j in range(len(nums) - i):
        res.append(nums[j])
    return res

###############################################################################
# Shifting and splicing O(n)


def rotatebyn(nums, i):
    return nums[-i:] + nums[:-i]


###############################################################################
# Without extra space and inline with O(1)


def rotateoptimized(nums, k):
    nums = deque(nums)
    nums.rotate(k)
    nums = list(nums)
    return nums


print(rotateoptimized([1, 2, 3, 4, 5], 2))
