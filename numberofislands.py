# Given a 2d grid map of '1's (land) and '0's (water), count the number of islands.
# An island is surrounded by water and is formed by connecting adjacent lands horizontally or vertically.
# You may assume all four edges of the grid are all surrounded by water.


def DFS(grid, i, j):
    if (i < 0 or j < 0 or i >= len(grid) or j >= len(grid[0]) or grid[i][j] != '1'):
        return
    grid[i][j] = '#'
    DFS(grid, i + 1, j)
    DFS(grid, i - 1, j)
    DFS(grid, i, j + 1)
    DFS(grid, i, j - 1)


def numIsIslands(grid):
    if not grid:
        return 0
    numOfIslands = 0
    for i in range(len(grid)):
        for j in range(len(grid[0])):
            if grid[i][j] == '1':
                DFS(grid, i, j)
                numOfIslands += 1
    return numOfIslands


print(numIsIslands([["1", "1", "1", "1", "0"], ["1", "1", "0", "1", "0"],
                    ["1", "1", "0", "0", "0"], ["0", "0", "0", "1", "0"]]))
