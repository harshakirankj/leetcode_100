# Reverse bits of a given 32 bits unsigned integer.


def reverseBits(x):
    x = int(x, 2)
    if x > 0:  # handle positive numbers
        a = int(str(x)[::-1])
    if x <= 0:  # handle negative numbers
        a = -1 * int(str(x*-1)[::-1])
    # handle 32 bit overflow
    mina = -2**31
    maxa = 2**31 - 1
    if a not in range(mina, maxa):
        return 0
    else:
        return bin(a)


print(reverseBits("11111111111111111111111111111101"))
