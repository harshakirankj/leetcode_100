def sortByparity(nums):
    i = 0
    for j in range(len(nums)):
        if nums[j] % 2 == 0:
            nums[i], nums[j] = nums[j], nums[i]
            i += 1
    return nums


print(sortByparity([3, 1, 2, 4]))
