# O(n^2)
def two_sum_on2(nums, target):
    res = []
    for i in range(len(nums)):
        for j in range(i+1, len(nums)):
            if nums[i] + nums[j] == target:
                res.append(i)
                res.append(j)
    return res


# O(n)
def two_sum_on1(nums, target):
    dic = {}
    for i, num in enumerate(nums):
        n = target - num
        if n in dic:
            return [dic[n], i]
        else:
            dic[num] = i


def three_sum_on1(nums, target):
    dic = {}
    for i, num in enumerate(nums):
        n = target - num
        isol = target - num - n
        if n in dic and isol in dic:
            return [dic[n], i, dic[isol]]
        else:
            dic[num] = i


print(three_sum_on1([2, 7, 11, 15], 20))
