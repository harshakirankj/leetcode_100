# move pointers fom lft and right.
# if they are equal we flip them and invert
# space complexity O(1)
# Time complexity O(n * m)


def flipandinvert(A):
    for i in range(len(A)):
        low = 0
        high = len(A) - 1
        while (low <= high):
            if (A[i][low] == A[i][high]):
                A[i][low] = 1 - A[i][low]
                A[i][high] = A[i][low]

            low += 1
            high -= 1

    return A


print(flipandinvert([[1, 1, 0], [1, 0, 1], [0, 0, 0]]))
