def minimumBribes(q):
    count = 0
    Q = [p-1 for p in q]
    for i, P in enumerate(Q):
        if(P - i) > 2:
            print("Too chaotic")
            return 0

    for i in range(len(Q)):
        for j in range(len(Q)-i-1):
            if Q[j] > Q[j+1]:
                Q[j], Q[j + 1] = Q[j + 1], Q[j]
                count += 1

    print(int(count))


minimumBribes([2, 1, 3, 6, 5, 4, 8, 7])
