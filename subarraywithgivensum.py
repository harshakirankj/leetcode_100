import collections

# test the theoury


def testfindarrys(nums, k):
    count = 0
    Sum = 0
    dic = collections.defaultdict(int)
    dic[0] += 1
    for num in nums:
        Sum += num
        count += dic[Sum - k]
        dic[Sum] += 1

    return count


# Sliding window O(n^2)


def subarraywithsum(nums, k):
    lp = 0
    count = 0
    for i in range(len(nums)):
        lp = i
        rp = lp + 1
        lsum = nums[lp]
        if (nums[i] == k):
            count += 1
        for rp in range(rp, len(nums)):
            lsum = lsum + nums[rp]
            if (lsum == k):
                count = count + 1

    return count


# O(n) with the hashmap appraoch

def subarraySum(nums, k):
    count = 0
    Sum = 0
    dic = collections.defaultdict(int)
    dic[0] += 1
    for num in nums:
        Sum += num
        count += dic[Sum - k]
        dic[Sum] += 1
    return count


print(subarraySum([0, 1, 1, 1, 0, 0, 0, 2], 2))
