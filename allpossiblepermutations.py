from itertools import permutations


def possiblepermutations(nums):
    return list(permutations(nums))


print(possiblepermutations([1, 2, 3]))
