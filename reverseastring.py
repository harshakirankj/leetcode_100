# in a loop
def reverse(s):
    str = ""
    for i in s:
        str = i + str
    return str

# in recursion


def reverse_recursion(s):
    if (len(s) == 0):
        return s
    else:
        return reverse_recursion(s[1:]) + s[0]

# in a stack


def reverse_stack(s):
    stack = []
    for i in range(len(s)):
        stack.append(s[i])

    string = ""

    for i in range(len(stack)):
        string += stack.pop()

    return string


s = 'harshakirankashajagadish'
print(reverse_recursion(s))
