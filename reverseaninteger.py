# Reverse an integer by converting it to a string!
def reverseInteger(x):

    if x > 0:  # handle positive numbers
        a = int(str(x)[::-1])
    if x <= 0:  # handle negative numbers
        a = -1 * int(str(x*-1)[::-1])
    # handle 32 bit overflow
    mina = -2**31
    maxa = 2**31 - 1
    if a not in range(mina, maxa):
        return 0
    else:
        return a


# Not converting it to a string
def reverseInteger_withoutstring(x):
    rev = 0
    negFlag = False

    if (x < 0):
        negFlag = True
        x = -x

    while (x > 0):
        rev = rev * 10 + x % 10
        x = int(x/10)

    # handle 32 bit overflow
    mina = -2**31
    maxa = 2**31 - 1
    if rev not in range(mina, maxa):
        return 0
    else:
        return -rev if negFlag else rev


print(reverseInteger_withoutstring(-120))
