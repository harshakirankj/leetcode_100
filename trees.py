class Node:
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def preOrder(node):
    if node:
        print(node.data)
        preOrder(node.left)
        preOrder(node.right)


def postOrder(node):
    if node:
        preOrder(node.left)
        preOrder(node.right)
        print(node.data)


def inOrder(node):
    if node:
        inOrder(node.left)
        print(node.data)
        inOrder(node.right)


def levelOrder(node):  # BFS
    ans = []  # Stack
    if node is None:
        return ans

    queue = []  # Queue
    queue.append(node)

    while(queue):
        root = queue.pop(0)   # pop from the queue
        ans.append(root.data)  # push in the stack
        if(root.left):
            queue.append(root.left)
        if(root.right):
            queue.append(root.right)

    return ans


def invertTree(root):
    if root:
        root.left, root.right = invertTree(root.right), invertTree(root.left)
        return root


def invertTreei(root):
    stack = [root]
    while stack:
        node = stack.pop(-1)
        if node:
            node.left, node.right = node.right, node.left
            stack.append(node.left)
            stack.append(node.right)
    return root


a = Node(1)
a.left = Node(4)
a.right = Node(5)
a.left.left = Node(11)
a.left.left.left = Node(15)
a.left.left.right = Node(16)
a.left.right = Node(17)
a.right.left = Node(6)
a.right.left.left = Node(66)
a.right.right = Node(7)
a.right.right.right = Node(77)

ia = invertTreei(a)
inOrder(ia)

