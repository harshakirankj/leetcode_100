from collections import Counter


def findLucky(arr):
    dic = Counter(arr)
    lucky = -1
    for i in dic:
        if i == dic[i]:
            lucky = max(lucky, i)
    return lucky


print(findLucky([2, 2, 3, 3, 3, 4]))
