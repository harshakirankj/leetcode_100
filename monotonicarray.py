

def isMonotonic_logic(A):
    increasing = decreasing = True

    for i in range(len(A) - 1):
        if A[i] > A[i+1]:
            increasing = False
        if A[i] < A[i+1]:
            decreasing = False

    return increasing or decreasing


def cmp(a, b):
    return (a > b) - (a < b)


def isMonotonic(A):
    store = 0
    for i in range(len(A) - 1):
        c = cmp(A[i], A[i+1])
        if c:
            if c != store != 0:
                return False
            store = c
    return True


def isMonotonic_hash(A):
    dic = {}
    for i in range(len(A)-1):
        if(A[i] - A[i+1]) < 0:
            dic['n'] += 1
        elif (A[i] - A[i+1]) > 0:
            dic['p'] += 1
        else:
            dic['z'] += dic.setdefault('z', None) + 1

    if (dic['n'] > 0 and dic['p'] > 0):
        return False
    else:
        return True


print(isMonotonic_hash([1, 1, 2, 3, 4, 5, 6, 8, 7]))
