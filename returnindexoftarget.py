def indexofpotentialpositionsearch(nums, target):
    for i in range(len(nums)):
        if target <= nums[i]:
            return i
    return len(nums)


print(indexofpotentialpositionsearch([1, 2, 3, 4, 5, 6, 7], 5))
