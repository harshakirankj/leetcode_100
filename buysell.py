# https://medium.com/algorithms-and-leetcode/best-time-to-buy-sell-stocks-on-leetcode-the-ultimate-guide-ce420259b323

# Using the Kadence Algorithm


def bysell(prices):
    n = len(prices)
    if n < 2:
        return 0
    maxprofit, minstock = 0, prices[0]

    for p in prices:
        maxprofit = max(maxprofit, p - minstock)
        minstock = min(minstock, p)
    return maxprofit

# Using sum of differences
# we can buy multiple (no upper limit) stocks to maximize the profit as opposed to only one in the previous


def solution(prices):
    n = len(prices)
    profit = 0
    for i in range(1, n):
        profit += max(prices[i] - prices[i - 1], 0)
    return profit


print(solution([1, 7, 2, 3, 6, 7, 6, 7]))
