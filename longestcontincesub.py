def findLengthOfLCIS(nums):
    ans = anchor = 0
    for i in range(len(nums)):
        if i and nums[i-1] >= nums[i]:
            anchor = i
        ans = max(ans, i - anchor + 1)
    return ans


print(findLengthOfLCIS([0, 1, 2, 3, 4, 5, 55, 8, 9, 11, 12, 13, 14]))
##############################################################################


def LIS(nums):
    if len(nums) == 0:
        return 0
    L = [1 for x in range(len(nums))]
    i, j = 1, 0
    while i < len(nums) and j < len(nums):
        if nums[i] > nums[j]:
            if L[j] + 1 > L[i]:
                L[i] = L[j] + 1
        j = j + 1
        if j == i:
            j, i = 0, i+1
    return max(L)


print(LIS([0, 7, 5, 11, 3, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15]))
##############################################################################


def NIS(nums):
    if len(nums) == 0:
        return 0
    L = [1 for x in range(len(nums))]
    i, j = 1, 0
    while i < len(nums) and j < len(nums):
        if nums[i] > nums[j]:
            if L[j] + 1 >= L[i]:
                L[i] = L[j] + 1
        j = j + 1
        if j == i:
            j, i = 0, i+1
    return max(L)


print(NIS([1, 4, 5, 6, 2, 2, 2, 2]))
##############################################################################


def lcs(X, Y):
    # find the length of the strings
    m = len(X)
    n = len(Y)

    # declaring the array for storing the dp values
    L = [[None]*(n + 1) for i in range(m + 1)]

    """Following steps build L[m + 1][n + 1] in bottom up fashion 
    Note: L[i][j] contains length of LCS of X[0..i-1] 
    and Y[0..j-1]"""
    for i in range(m + 1):
        for j in range(n + 1):
            if i == 0 or j == 0:
                L[i][j] = 0
            elif X[i-1] == Y[j-1]:
                L[i][j] = L[i-1][j-1]+1
            else:
                L[i][j] = max(L[i-1][j], L[i][j-1])

    # L[m][n] contains the length of LCS of X[0..n-1] & Y[0..m-1]
    print(L)
    return L[m][n]
# end of function lcs


# Driver program to test the above function
X = "AGGTAB"
Y = "GXTXAYB"
print("Length of LCS is ", lcs(X, Y))
