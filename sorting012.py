# sort array of 0,1,2 without using the sort function
from collections import Counter


def sort012_brute(nums):
    dic = Counter(nums)
    count0 = dic[0]
    count1 = dic[1]
    count2 = dic[2]
    for i in range(len(nums)):
        nums.pop()
    for i in range(count0):
        nums.append(0)
    for i in range(count1):
        nums.append(1)
    for i in range(count2):
        nums.append(2)
    return nums


def sort012_ganta_notworking(lst):
    dic = Counter(lst)
    res = []
    for i in range(len(dic)):

        for j in range(dic[i]):
            res.append(i)
    return res


def sort012(nums):
    le = len(nums)
    low = 0
    mid = 0
    hi = le - 1

    while (mid <= hi):
        if nums[mid] == 0:
            nums[low], nums[mid] = nums[mid], nums[low]
            low = low + 1
            mid = mid + 1
        elif nums[mid] == 1:
            mid = mid + 1
        else:
            nums[mid], nums[hi] = nums[hi], nums[mid]
            hi = hi - 1

    return nums


print(sort012([1, 2, 1, 2, 1, 0, 0]))
