
from collections import Counter


def isomorphicstrings(s, t):
    sc = Counter(s)
    tc = Counter(t)
    if len(s) != len(t):
        return False

    skeys = list(sc.keys())
    svals = list(sc.values())
    tkeys = list(tc.keys())
    tvals = list(tc.values())

    if sorted(svals) != sorted(tvals):
        return False
    val = 0
    for i in range(len(skeys)):
        val += s[i] ^ t[i]

    return True


print(isomorphicstrings('aba', 'baa'))
print(isomorphicstrings('egg', 'add'))
print(isomorphicstrings('foo', 'bar'))
print(isomorphicstrings('paper', 'title'))
