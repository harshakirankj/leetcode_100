# narrow down the search  : divide and conquer
# Recursive approach


def rBinarySearch(nums, target):
    if len(nums) == 1:
        return target == nums[0]
    mid = int(len(nums)/2)
    if nums[mid] > target:
        return rBinarySearch(nums[: mid], target)
    if nums[mid] < target:
        return rBinarySearch(nums[mid:], target)
    return True

# iterate

# return the index
# log(n)


def iBinarySearch(nums, target):
    left = 0
    n = len(nums)
    right = n - 1
    while(left <= right):
        mid = int((left + right) / 2)
        if nums[mid] == target:
            return mid
        elif target < nums[mid]:
            right = mid - 1
        else:
            left = mid + 1
    return -1


def iFindPivot(nums):
    left = 0
    right = len(nums) - 1
    while(left <= right):
        mid = int((left + right) / 2)
        if (nums[mid] == 0 or nums[mid] < nums[mid - 1]):
            return mid
        elif nums[mid] >= nums[0]:
            left = mid + 1
        else:
            right = mid - 1
    return 0


def shiftedarraysearch(nums, target):
    pivot = iFindPivot(nums)
    if pivot == 0 or target < nums[0]:
        val = iBinarySearch(nums[pivot:], target)
        if val == -1:
            return val
        else:
            return pivot + val
    else:
        return iBinarySearch(nums[:pivot], target)


print(shiftedarraysearch([3, 2], 2))
