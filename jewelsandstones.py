from collections import Counter


def numJewelsInStones(J, S):
    dicJ = Counter(list(J))
    dicS = Counter(list(S))
    count = 0

    for x in dicJ:
        if x in dicS:
            count = count + dicS[x]

    return count


print(numJewelsInStones("z", "ZZ"))
