class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None


class LinkedList:
    def __init_(self):
        self.head = None

    def printList(self):
        node = self.head
        while(node is not None):
            print(node.data)
            node = node.next

    def reverseList(self):
        head = self.head
        new_head = None
        while(head is not None):
            tmp = head.next
            head.next = new_head
            new_head = head
            head = tmp
        return new_head


a = LinkedList()
a.head = Node(1)
e1 = Node(2)
e2 = Node(3)
e3 = Node(4)
e4 = Node(5)
a.head.next = e1
e1.next = e2
e2.next = e3
e3.next = e4
a.head = a.reverseList()
print(a.printList())
