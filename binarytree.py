class Node:
    def __init__(self, data):
        self.left = None
        self.right = None
        self.data = data

    def insert(self, data):
        if self.data:
            if data < self.data:
                if self.left is None:
                    self.left = Node(data)
                else:
                    self.left.insert(data)
            else:
                if self.right is None:
                    self.right = Node(data)
                else:
                    self.right.insert(data)
        else:
            self.data = data

    def printtree(self):
        if self.left:
            self.left.printtree()
        print(self.data)
        if self.right:
            self.right.printtree()

    def findval(self, data):
        if data < self.data:
            if self.left is None:
                return str(data) + " :NOT FOUND"
            return self.left.findval(data)
        elif data > self.data:
            if self.right is None:
                return str(data) + " :NOT FOUND"
            return self.right.findval(data)
        else:
            return str(data) + " :FOUND"


root = Node(1)
root.insert(6)
root.insert(4)
root.insert(10)
root.insert(56)

root.printtree()
print(root.findval(22))
