from collections import Counter


def positionorigin(moves):
    x = 0
    y = 0
    for i in moves:
        if i == 'U':
            y = y + 1
        elif i == 'L':
            x = x - 1
        elif i == 'R':
            x = x + 1
        elif i == 'D':
            y = y - 1

    return True if x == 0 and y == 0 else False


def judgeCircle(self, moves):
    """
    :type moves: str
    :rtype: bool
    """

    if not moves:
        return True

    if moves.count('U') == moves.count('D') and moves.count('L') == moves.count('R'):
        return True
    else:
        return False


def judgeCirclse(self, moves):
    c = Counter(moves)
    return c['U'] == c['D'] and c['L'] == c['R']


print(positionorigin("LL"))
