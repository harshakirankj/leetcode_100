
# Merge sort of 2 sorted arrays algorithm


def merge(left, right):
    i, j = 0, 0
    result = []
    while i < len(left) and j < len(right):
        if (left[i] < right[j]):
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1

    while i < len(left):
        result.append(left[i])
        i += 1

    while j < len(right):
        result.append(right[j])
        j += 1

    return result


print(merge([1, 3, 4], [0, 2, 3, 5, 6, 7, 10]))
