from collections import Counter

# with Extra memory O(n)


def removeduplicates(nums):
    dic = Counter(nums)
    res = []
    for x, i in dic.items():
        if x not in res:
            res.append(x)
    return res


def removeduplicates_inplace(nums):
    i = 0
    while i < len(nums) - 1:
        if (nums[i] == nums[i + 1]):
            del nums[i]
        else:
            i += 1
    return nums


print(removeduplicates_inplace([1, 1, 2, 4, 4, 4, 4, 4, 4, 4, 6, 6, 6]))
