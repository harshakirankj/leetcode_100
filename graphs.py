# DFS
# Using a Python dictionary to act as an adjacency list
graph = {
    'A': ['B', 'C'],
    'B': ['D', 'E'],
    'C': ['F'],
    'D': [],
    'E': ['F'],
    'F': []
}

visited = set()  # Set to keep track of visited nodes.


def dfs(visited, graph, node):
    if node not in visited:
        print(node)
        visited.add(node)
        for neighbour in graph[node]:
            dfs(visited, graph, neighbour)


# Driver Code
dfs(visited, graph, 'A')

#############################################################
visitedb = []  # List to keep track of visited nodes.
queue = []  # Initialize a queue


def bfs(visitedb, graph, node):
    visitedb.append(node)
    queue.append(node)

    while queue:
        s = queue.pop(0)
        print(s, end=" ")

        for neighbour in graph[s]:
            if neighbour not in visitedb:
                visitedb.append(neighbour)
                queue.append(neighbour)


# Driver Code
bfs(visitedb, graph, 'A')
