def equilibriumarray(nums):
    leftsum = 0
    rightsum = 0

    for i in range(len(nums)):
        leftsum = sum(nums[:i])
        rightsum = sum(nums[i+1:])
        if leftsum == rightsum:
            return i

    return -1


def pivotIndex(nums):
    S = sum(nums)
    leftsum = 0
    for i, x in enumerate(nums):
        if leftsum == (S - leftsum - x):
            return i
        leftsum += x
    return -1


print(pivotIndex([1, 2, 3]))
