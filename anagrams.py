# Check if the given string is an anagram or not
# Ex: Elvis levis are anagrams
from collections import Counter

# using sorting


def checkAnagram_sort(str1, str2):
    return sorted(str1) == sorted(str2)

# using Counter from Dictionary


def checkAnagram_counter(str1, str2):
    return Counter(str1) == Counter(str2)

# naive counter implmentation


def createCounter(str):
    ret = {}
    for e in str:
        ret[e] = ret.setdefault(e, 0) + 1
    return ret


def checkAnagram_naivecounter(str1, str2):
    return createCounter(str1) == createCounter(str2)


print(checkAnagram_naivecounter('aabc', 'abac'))
