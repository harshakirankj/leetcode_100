def constructLmatrix(matrix, mid):
    res = []
    for i in range(len(matrix)):
        res.append(matrix[i][:mid])
    return res


def constructRmatrix(matrix, mid):
    res = []
    for i in range(len(matrix)):
        res.append(matrix[i][mid:])
    return res


def searchMatrix(matrix, target):
    """
    :type matrix: List[List[int]]
    :type target: int
    :rtype: bool
    """
    if len(matrix) == 2:
        return target == matrix[0][0]
    mid = int(len(matrix)/2)

    if(target < matrix[mid][mid]):
        searchMatrix(constructLmatrix(matrix, mid), target)
    else:
        searchMatrix(constructRmatrix(matrix, mid), target)


def search(mat, x):
    i = 0
    n = len(mat)
    j = n - 1
    while (i < n and j >= 0):
        val = mat[i][j]
        if (val == x):
            print("n Found at ", i, ", ", j)
            return 1

        if (val > x):
            j -= 1

        # if mat[i][j] < x
        else:
            i += 1

    print("Element not found")


print(search([[1, 4, 7, 11, 15], [2, 5, 8, 12, 19], [
      3, 6, 9, 16, 22], [10, 13, 14, 17, 24], [18, 21, 23, 26, 30]], 19))
