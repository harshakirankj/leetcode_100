# {{[{()[]}]}}

# Approach#3 : Elimination based
#Ineveryiteration, theinnermostbracketsgeteliminated
# (replaced with empty string). If we end up with an empty string, our initial one was balanced; otherwise, not.


def valid_paranthesis(s):
    pairs = ['{}', '[]', '()']
    while any(x in s for x in pairs):
        for b in pairs:
            s = s.replace(b, '')
    return not s


print(valid_paranthesis("{{[{()[]}]}}"))


def i_longest_valid_parenthesis(s):
    count = 0
    while ('()' in s):
        s = s.replace('()', '', 1)
        count += 2
    return count


def min_add_to_make_valid(S):
    while ('()' in S):
        S = S.replace('()', '')
    return len(S)


def longest_valid_parenthesis(s):
    longest = 0
    length = 0
    stack = []
    for char in s:
        if char == '(':
            stack.append(length)
            length = 0
        else:
            if len(stack) > 0:
                length = stack.pop() + length + 2
                longest = max(longest, length)
            else:
                # Does not create a valid substring
                length = 0
    return longest


def longeststring(s):
    longest = 0
    stack = []
    length = 0

    for i in s:
        if i == '(':
            stack.append(length)
            length = 0
        else:
            if len(stack) > 0:
                length = stack.pop() + length + 2
                longest = max(longest, length)
            else:
                length = 0
    return longest


print(longeststring('())))))))'))
