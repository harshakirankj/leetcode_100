# Gives the max sub array sum in a given array

# STEP -01 : ue localsum and globalsum
# STEP -02 : localSum = max(nums[i], localSum + nums[i])
#            globalSum = max(localSum, globalSum)
#
# STEP -03: return globalSum
# https://medium.com/@rsinghal757/kadanes-algorithm-dynamic-programming-how-and-why-does-it-work-3fd8849ed73d


def maxsuminarray(nums):
    localsum = 0
    globalsum = 0
    for i in nums:
        localsum = max(i, localsum + i)
        globalsum = max(localsum, globalsum)

    return globalsum


def buysell(prices):
    n = len(prices)
    if n < 2:
        return 0
    maxprofit, minstock = float('-inf'), prices[0]
    for p in prices:
        maxprofit = max(maxprofit, p - minstock)
        minstock = min(minstock, p)
    return maxprofit


print(maxsuminarray([1, 2, 3, 195, -2, -5, 6, 7, -2, 0, 34, 56, 77, 3]))
