# find the missing number in the array starting from 0
# Trick is XOR.
# XOR of same values is 0
# XOR of different values is 1


def findmissing(nums):
    val = 0
    for i in range(len(nums) + 1):
        val = val ^ i

    for i in range(len(nums)):
        val = val ^ nums[i]

    return val


print(findmissing([1, 3, 4, 5]))
