# Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap
# after raining.

#Input: [0,1,0,2,1,0,1,3,2,1,2,1]


def areaoftrappedwater(height):
    area = 0
    n = len(height)

    for i in range(1, n-1):
        left = height[i]
        for j in range(i):
            left = max(left, height[j])

        right = height[i]
        for j in range(i + 1, n):
            right = max(right, height[j])

        area = area + min(left, right) - height[i]
    return area


print(areaoftrappedwater([0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]))
