def canijumppass(nums):
    lastGoodvalueIndex = len(nums) - 1
    i = len(nums) - 1
    while i >= 0:
        if (i + nums[i] >= lastGoodvalueIndex):
            lastGoodvalueIndex = i
        i -= 1

    return lastGoodvalueIndex == 0


print(canijumppass([3, 2, 1, 1, 4]))
